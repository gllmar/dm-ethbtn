# dm-ethbtn

[distributed-memories](https://gitlab.com/gllmar/distributed-memories) ethernet button


## Intégration 
 
Plaquette de 15cm par 15cm dans un cube de plexi de 20cm
![ethbtn2_integration](./medias/integration.png)


### Bouton 
![ethbtn2_19mm](./medias/bouton_19mm.png)

### Connecteur 

![](./medias/RCP-5SPFFV-TCU7001_drawio.png)

[fiche_technique](./medias/RCP-5SPFFV-TCU7001.pdf)

![RCP-5SPFFV-TCU7001](./medias/RCP-5SPFFV-TCU7001.webp)

### Dimensions

Plaquette de 15cm par 15cm dans un cube de plexi de 20cm

### Branchement

#### Bouton vers USB plug male

```mermaid
flowchart LR
    subgraph Bouton
        bouton_ledvcc(LED VCC : Rouge)
        bouton_common(Commun : Vert)
        bouton_no( NO : Bleu )
        bouton_ledgnd(LED GND : Noir )
        bouton_nc(NC : Jaune)
    end
    subgraph USB_male
        usb_m_vbus(v+ : Rouge)
        usb_m_d_moins(D- : Blanc )
        usb_m_d_plus(D+ : Vert )
        usb_m_gnd(gnd: Noir)
    end
    bouton_ledvcc-->usb_m_vbus
    bouton_ledgnd-->usb_m_gnd
    bouton_no-->usb_m_d_moins
    bouton_common-->usb_m_d_plus

```

#### M5 vers Usb socket

```mermaid
flowchart LR
    subgraph M5
        M5_GND(GND:NOIR)
        M5_VCC(VCC:ROUGE)
        M5_26(26:JAUNE)
        M5_32(32:BLANC)
    end
    subgraph PWM_CTL
        PWM_VIN_PLUS
        PWM_VIN_MINUS
        PWM_TRIG
        PWM_VOUT_PLUS
        PWM_VOUT_MINUS
        PWM_GND
    end
    subgraph USB_SOCKET
        USB_VBUS
        USB_D_MINUS
        USB_D_PLUS
        USB_GND
    end

    M5_32-->10kΩ-->PWM_VIN_PLUS
    M5_32-->USB_D_PLUS
    M5_26-->PWM_TRIG
    M5_VCC-->PWM_VIN_PLUS
    M5_GND-->PWM_VIN_MINUS

    PWM_GND-->USB_D_MINUS
    PWM_VOUT_PLUS-->USB_VBUS
    PWM_VOUT_MINUS-->USB_GND

```








## V1
Implémentation 2 vise à réduire un problème de surchauffe présent dans la V1

* Documentation V1 https://github.com/gllmar/ethbtn
* Boitier 



## Features TODO

* DHCP
* Broadcast
* OSC Button output
* PWM wave
* OSC parameter setup (written to eeprom) 
