/*
distibuted-memories 
implementation bouton osc broadcast
*/
//  OSC MESSAGE
char * osc_prefix="/dm/ethbtn/btn";
#define OSC_OUT_PORT 52022
//



// ethPOE
#include <SPI.h>
#include <Ethernet2.h>


#define SCK 22
#define MISO 23
#define MOSI 33
#define CS 19

byte mac[] = { 0xDE, 0xAD, 0xBA, 0xEF, 0xFE, 0xEE };
IPAddress ip_broadcast;

// \ethPOE
// osc
#include <OSCMessage.h>
#include <EthernetUdp2.h>
EthernetUDP udp;
const unsigned int osc_out_port = OSC_OUT_PORT;

// \osc


// m5 LED
#include "M5Atom.h"

// couleur du led
#define RED 0xff, 0x00, 0x00
#define YELLOW 0xff, 0xff, 0x00
#define GREEN 0x00, 0xff, 0x00
#define CYAN 0x00, 0xff, 0xff
#define BLUE 0x00, 0x00, 0xff
#define PINK 0xff, 0x00, 0xff
#define OFF 0x00, 0x00, 0x00


//  btn externe
#define btn_pin 32
Button bouton = Button(btn_pin, false, 100);

// led control
#define LEDC_CHANNEL_0     0
#define LEDC_TIMER_12_BIT  12 // use 12 bit precission for LEDC timer
#define LEDC_BASE_FREQ     5000 // use 5000 Hz as a LEDC base frequency


// fade LED PIN (replace with LED_BUILTIN constant for built-in LED)
#define LED_PIN            26

int brightness = 0;    // how bright the LED is
int fadeAmount = 1;    // how many points to fade the LED by
int interval= 10; // delay to dim without delay millis
long unsigned int led_next_millis =0;

// Arduino like analogWrite
// value has to be between 0 and valueMax
void ledcAnalogWrite(uint8_t channel, uint32_t value, uint32_t valueMax = 255) {
  // calculate duty, 4095 from 2 ^ 12 - 1
  uint32_t duty = (4095 / valueMax) * min(value, valueMax);

  // write duty to LEDC
  ledcWrite(channel, duty);
}



uint8_t DisBuff[2 + 5 * 5 * 3];
void set_m5_led(uint8_t Rdata, uint8_t Gdata, uint8_t Bdata)
{
    DisBuff[0] = 0x05;
    DisBuff[1] = 0x05;
    for (int i = 0; i < 25; i++)
    {
        DisBuff[2 + i * 3 + 1] = Rdata; // -> fix : grb? weird?
        DisBuff[2 + i * 3 + 0] = Gdata; //->  fix : grb? weird? 
        DisBuff[2 + i * 3 + 2] = Bdata;
    }
      M5.dis.displaybuff(DisBuff);
}

// \m5 LED



void setup() {
  M5.begin(true, false, true);
  set_m5_led(RED);
  setup_led();
  

  SPI.begin(SCK, MISO, MOSI, -1);
  Ethernet.init(CS);

  // init DHCP
  Ethernet.begin(mac);
  udp.begin(8888);
  set_m5_led(PINK); // IF here; ethernet initialize
  

  // Create broadcast IP
  ip_broadcast = Ethernet.localIP();
  ip_broadcast[3]=255;

  // Print init state
  Serial.print("Button ip => ");
  Serial.print(Ethernet.localIP());
  Serial.print(" broadcasting => ");
  Serial.println(ip_broadcast);

}

void loop() 
{
  // listen for incoming clients
   M5.update();
   bouton.read();
   
   unsigned long int time_now = millis();
   
   if(led_next_millis<time_now)
   {
      led_next_millis=time_now+interval;
      brightness = brightness + fadeAmount;
        if (brightness <= 0 || brightness >= 253) 
          {
            fadeAmount = -fadeAmount;
          }
      ledcAnalogWrite(LEDC_CHANNEL_0, brightness);
   }   
  
   if (M5.Btn.wasPressed())
   {
    Serial.println("BTN");
    send_osc_btn(1);
    cycle_m5_color(200);
   }

   if(bouton.wasPressed())
   {
     send_osc_btn(1);

   }
   if(bouton.wasReleased())
   {
       send_osc_btn(0);

   }
   if(bouton.isPressed())
   {
            brightness=253;

   } else {

   }
    
}


void send_osc_btn(bool b_state)
{
  //Serial.println(OSC_PREFIX); 
  OSCMessage msg(osc_prefix);
  msg.add((int32_t)b_state); 
  msg.add(((const char *)"time"));
  msg.add(((int32_t)millis()));
  udp.beginPacket(ip_broadcast, osc_out_port);
  msg.send(udp); // send the bytes to the SLIP stream
  udp.endPacket(); // mark the end of the OSC Packet
  msg.empty(); // free space occupied by message
}

void setup_led()
{
  ledcSetup(LEDC_CHANNEL_0, LEDC_BASE_FREQ, LEDC_TIMER_12_BIT);
  ledcAttachPin(LED_PIN, LEDC_CHANNEL_0);
  ledcAnalogWrite(LEDC_CHANNEL_0, brightness);
  delay(50);
  brightness=255;
  ledcAnalogWrite(LEDC_CHANNEL_0, brightness);
  delay(50);
  brightness=0;
  ledcAnalogWrite(LEDC_CHANNEL_0, brightness);
}

void cycle_m5_color(int d_time)
{
  delay(d_time);
  set_m5_led(RED);
  delay(d_time);
  set_m5_led(YELLOW);
  delay(d_time);
  set_m5_led(GREEN);
  delay(d_time);
  set_m5_led(CYAN);
  delay(d_time);
  set_m5_led(BLUE);
  delay(d_time);
  set_m5_led(PINK);
}
